package main

import (
	"bexs-backend-exam/handler"
	"bexs-backend-exam/router"
	"bufio"
	"context"
	"fmt"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Fprintf(os.Stderr, "incorrect usage: you must supply a filename as the firs argument\n")
		os.Exit(1)
	}

	// open and load the routes file
	file, err := os.Open(os.Args[1])
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	r := router.New()
	if err := r.LoadFrom(file); err != nil {
		fmt.Fprintln(os.Stderr, err)

		if closeErr := file.Close(); closeErr != nil {
			fmt.Fprintln(os.Stderr, closeErr)
		}

		os.Exit(1)
	}

	if err := file.Close(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	// http handler
	var wg sync.WaitGroup

	h := handler.New(os.Args[1], r)
	http.HandleFunc("/routes/", h.AddRoute)
	http.HandleFunc("/routes/best/", h.FindRoute)

	srv := &http.Server{Addr: ":8080"}
	wg.Add(1)

	go func() {
		defer wg.Done()

		if err := http.ListenAndServe(":8080", nil); err != http.ErrServerClosed {
			fmt.Fprintln(os.Stderr, err)
		}
	}()

	// read from stdin and execute the query
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Fprintf(os.Stdout, "please enter the route: ")

	for scanner.Scan() {
		line := strings.TrimSpace(scanner.Text())
		fields := strings.Split(line, "-")

		if len(fields) != 2 {
			fmt.Fprintln(os.Stderr, "Wrong route input. Try 'nodeA-NodeB'.")
			fmt.Fprintf(os.Stdout, "please enter the route: ")
			continue
		}

		route, cost, err := r.GetRoute(fields[0], fields[1])
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			fmt.Fprintf(os.Stdout, "please enter the route: ")
			continue
		}

		fmt.Fprintf(os.Stderr, "best route: %s > $%d\n", strings.Join(route, " - "), cost)
		fmt.Fprintf(os.Stdout, "please enter the route: ")
	}

	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	// try to shutdown the http server gracefully
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)

	if err := srv.Shutdown(ctx); err != nil {
		fmt.Fprintln(os.Stderr, err)
		cancel()
		os.Exit(1)
	}

	cancel()
	wg.Wait()
}
