# Bexs Challenge - "Rota de Viagem"

**IMPORTANT:** If you wish more details, please check the [original README file](./README.original.md).

## Compiling and running from source

Assuming you have a golang compiler available, just run `go build` on the root directory. This will create the `bexs-backend-exam` binary, which you can run to see the result. The console UI is as specified on the [original README file](./README.original.md). By default, the port 8080 is used to expose the available HTTP endpoints, that are listed on the [Available Endpoints](#available-endpoints) section.

## Compiling and running from docker

If you don't have a golang compiler available, you can still run the application using docker:

```bash
# First, build the image
$ docker build -t bexs-backend-exam .

# run the application.
# keep in mind the '-it', to be able to use the console UI
$ docker run --rm -it -p 8080:8080 bexs-backend-exam
```

The console UI works as defined on the [original README file](./README.original.md), while the HTTP interface is defined according to the [Available Endpoints](#available-endpoints) section below.

## Available endpoints

### Defining a new route

To define a new route, the endpoint `POST /routes/` is available (mind the slash at the end!). This endpoint accepts a JSON body with the `from`, `to` and `cost` fields and returns a JSON message with `"OK"` if the request is successfull. Example:

```bash
$ curl --location --request POST 'http://localhost:8080/routes/' \
--header 'Content-Type: application/json' \
--data-raw '{
    "from": "CDG",
    "to": "SCL",
    "cost": 30
}'
{"message":"OK","isError":false}
```

### Finding the best route

To find the best route, just do a `GET /routes/best/{from}-{to}`. For example:

```bash
# finding the best route
$ curl --location --request GET 'http://localhost:8080/routes/best/GRU-CDG'
{"path":["GRU","BRC","SCL","ORL","CDG"],"cost":40}

# what if the location is not reachable?
$ curl --location --request GET 'http://localhost:8080/routes/best/CDG-SCL'
{"message":"unreachable node","isError":true}
```

## How the code is organized

The code is organized as simple as possible; the `router` package has the entire graph and routing logic, while the `handler` package holds the HTTP endpoints. Everything is joined at the (admittedly, somewhat convoluted) `main.go`.

The real "fun" is at the `router/graph.go` file. This file defines a graph with a set of nodes (check `router/set.go`) and maps the edges "from" a node "to" a node with an specified cost. The `Graph` struct also has the all-important `Dijkstra` method, that computes the shortest path from a given node to all other (reachable) nodes and returns it as a `DijkstraResult` (see `router/dijkstra.go`). The `DijkstraResult` is nothing more than a "table" with the accumulated const to reach each node.

The `Router` struct (`router/router.go`) builds on top of `Graph` and `DijkstraResult` by caching the results (so, repeated queries from the same origin will not have to run the shortest path algorith again), also adding the option to load or save the graph data to a stream.

The code of `main.go` and `handler/handler.go` are basically boilerpart code, really.

## Caveats

Since the main "problem" of this challenge is the shortest path calculation, I didn't added a lot of the possible validations, specially on the endpoints. The reason for this is that it wasn't explicitly asked and the pain of using the standard `http` library is already too great (you guys said 'no dependencies'). This means that if you try to input broken data like negative costs you **WILL** break the system.

Of course, in a *real-world* scenario I would use an actual http library (e. g. [chi](https://github.com/go-chi/chi), [echo](https://github.com/labstack/echo), [mux](https://github.com/gorilla/mux) or something like that) and create proper validation (and tests, of course).
