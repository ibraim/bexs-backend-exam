package router_test

import (
	"bexs-backend-exam/router"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strings"
	"testing"
)

func TestRouterLoadSave(t *testing.T) {
	expectedRoutes := []string{"GRU", "BRC", "SCL", "ORL", "CDG"}
	expectedCost := int64(40)

	r := router.New()
	file, err := os.Open("testdata/input.csv")
	if err != nil {
		t.Fatal(err)
	}
	defer file.Close()

	if err := r.LoadFrom(file); err != nil {
		t.Fatal(err)
	}

	tempfile, err := ioutil.TempFile("", "")
	if err != nil {
		t.Fatal(err)
	}
	defer tempfile.Close()

	if err := r.SaveTo(tempfile); err != nil {
		t.Fatal(err)
	}

	tempfile.Close()
	r = router.New()
	file2, err := os.Open("testdata/input.csv")
	if err != nil {
		t.Fatal(err)
	}
	defer file2.Close()

	if err := r.LoadFrom(file2); err != nil {
		t.Fatal(err)
	}

	routes, cost, err := r.GetRoute("GRU", "CDG")
	if err != nil {
		t.Fatal(err)
	}

	if !equal(routes, expectedRoutes) {
		t.Fatalf("Expected routes to be '%v' but received '%v'", expectedRoutes, routes)
	}

	if cost != expectedCost {
		t.Fatalf("Expected cost to be %d, but received %d", expectedCost, cost)
	}
}

func TestRouterLoadError(t *testing.T) {
	tests := []struct {
		name        string
		reader      io.Reader
		expectedErr string
	}{
		{name: "Wrong size 1", reader: strings.NewReader("a,b"), expectedErr: "incorrect field numbers on line 'a,b'"},
		{name: "Wrong size 2", reader: strings.NewReader("a,b,c,d,e"), expectedErr: "incorrect field numbers on line 'a,b,c,d,e'"},
		{name: "Wrong number", reader: strings.NewReader("a,b,x"), expectedErr: "error parsing line 'a,b,x': strconv.ParseInt: parsing \"x\": invalid syntax"},
	}

	for _, test := range tests {
		test := test

		t.Run(test.name, func(t *testing.T) {
			r := router.New()
			err := r.LoadFrom(test.reader)
			if err == nil {
				t.Fatal("Expected error, but none found")
			}

			if err.Error() != test.expectedErr {
				t.Fatalf("Expected error '%s', but received '%s'", test.expectedErr, err)
			}
		})
	}
}

func TestRouterRoutes(t *testing.T) {
	tests := []struct {
		from           string
		to             string
		expectedRoutes []string
		expectedCost   int64
		expectedErr    string
	}{
		{from: "GRU", to: "CDG", expectedRoutes: []string{"GRU", "BRC", "SCL", "ORL", "CDG"}, expectedCost: 40},
		{from: "CDG", to: "BRC", expectedRoutes: []string{"CDG", "BRC"}, expectedCost: 20},
		{from: "CDG", to: "SCL", expectedRoutes: []string{"CDG", "BRC", "SCL"}, expectedCost: 25},
		{from: "ORL", to: "GRU", expectedErr: "unreachable node"},
	}

	for _, test := range tests {
		test := test

		t.Run(fmt.Sprintf("%s->%s", test.from, test.to), func(t *testing.T) {
			r := router.New()
			r.Add("GRU", "BRC", 10)
			r.Add("BRC", "SCL", 5)
			r.Add("GRU", "CDG", 75)
			r.Add("GRU", "SCL", 20)
			r.Add("GRU", "ORL", 56)
			r.Add("ORL", "CDG", 5)
			r.Add("SCL", "ORL", 20)

			r.Add("CDG", "BRC", 20)
			r.Add("CDG", "SCL", 30)

			routes, cost, err := r.GetRoute(test.from, test.to)
			if test.expectedErr != "" {
				if err == nil {
					t.Fatalf("Expected error, but none got")
				}

				if err.Error() != test.expectedErr {
					t.Fatalf("Expected error to be '%s', but it was '%s'", test.expectedErr, err)
				}

				return
			}

			if err != nil {
				t.Fatal(err)
			}

			if !equal(routes, test.expectedRoutes) {
				t.Fatalf("Expected routes to be '%v' but received '%v'", test.expectedRoutes, routes)
			}

			if cost != test.expectedCost {
				t.Fatalf("Expected cost to be %d, but received %d", test.expectedCost, cost)
			}
		})
	}
}
