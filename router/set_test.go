package router_test

import (
	"bexs-backend-exam/router"
	"testing"
)

func TestSetAddRemove(t *testing.T) {
	set := router.NewSet()
	items := []string{"foo", "bar", "baz"}

	for _, item := range items {
		set.Add(item)
	}

	if set.Size() != len(items) {
		t.Fatalf("Expected set of size %d, but received %d", len(items), set.Size())
	}

	for _, item := range items {
		if !set.Has(item) {
			t.Fatalf("Should have found '%s' on the set", item)
		}
	}

	slice := set.Slice()
	if set.Size() != len(slice) {
		t.Fatalf("Wrong slice size; expected %d but got %d", set.Size(), len(slice))
	}

	for _, item := range items {
		set.Remove(item)
	}

	if set.Size() != 0 {
		t.Fatalf("Size should be zero; instead, received %d", set.Size())
	}

	for _, item := range items {
		if set.Has(item) {
			t.Fatalf("Should not have found '%s' on the set", item)
		}
	}
}
