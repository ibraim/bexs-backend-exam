package router

import (
	"errors"
	"math"
)

// ErrUnreachable is the error returned when a node is unreachable.
var ErrUnreachable = errors.New("unreachable node")

type dijkstraItem struct {
	parent string
	cost   int64
}

// DijkstraResult is a result set with the shortest path to a certain node.
// As the name suggests, this uses the Dijkstra algorithm to calculate the
// paths from a given node.
//
// To create a DijkstraResult, use the 'Dijkstra' method of the Graph struct.
type DijkstraResult struct {
	data map[string]dijkstraItem
}

// To returns the shortest path and the total cost to a target node, or
// error.
func (dr *DijkstraResult) To(targetNode string) ([]string, int64, error) {
	item, ok := dr.data[targetNode]
	if !ok || item.cost == math.MaxInt64 {
		return nil, 0, ErrUnreachable
	}

	cost := item.cost
	path := []string{targetNode}

	for item.parent != "" {
		path = append(path, item.parent)
		item = dr.data[item.parent]
	}

	// reverse the path
	reverse := make([]string, 0, len(path))
	for i := len(path) - 1; i >= 0; i-- {
		reverse = append(reverse, path[i])
	}

	return reverse, cost, nil
}
