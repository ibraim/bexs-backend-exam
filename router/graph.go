package router

import (
	"math"
	"sort"
)

// Graph is a (unidirectional) graph of nodes and edges.
// This structure encodes a list of routes and the cost of
// traveling between nodes.
type Graph struct {
	nodes *Set
	edges map[string]map[string]int
}

// NewGraph returns a new, empty graph.
func NewGraph() *Graph {
	return &Graph{
		nodes: NewSet(),
		edges: make(map[string]map[string]int),
	}
}

// Add adds and edge to the graph, from one node to another.
// The edge is unidirectional, from nodeA to nodeB, and if it already exists,
// it is replaced with the new cost.
func (g *Graph) Add(nodeA string, nodeB string, cost int) {
	g.nodes.Add(nodeA)
	g.nodes.Add(nodeB)

	edges := g.edges[nodeA]
	if edges == nil {
		edges = make(map[string]int)
		g.edges[nodeA] = edges
	}

	edges[nodeB] = cost
}

// Nodes return the list of npdes pf the graph.
func (g *Graph) Nodes() []string {
	return g.nodes.Slice()
}

// EdgesOf returns the edges (and the associated cost) from a given node.
// It might return nil, if there are no edges from a given node.
func (g *Graph) EdgesOf(node string) map[string]int {
	return g.edges[node]
}

// Dijkstra return the shortest path from node to all other nodes,
// using the standard 'Dijkstra shortest path' algorithm.
func (g *Graph) Dijkstra(origin string) *DijkstraResult {
	type pendingItem struct {
		node   string
		parent string
		cost   int64
	}

	data := make(map[string]dijkstraItem)
	processed := NewSet()
	pending := []pendingItem{{node: origin, parent: "", cost: 0}}

	for _, node := range g.Nodes() {
		data[node] = dijkstraItem{cost: math.MaxInt64}
	}

	for len(pending) > 0 {
		sort.Slice(pending, func(i, j int) bool {
			return pending[i].cost < pending[j].cost
		})

		current := pending[0]
		if current.cost < data[current.node].cost {
			data[current.node] = dijkstraItem{parent: current.parent, cost: current.cost}
		}

		edges := g.EdgesOf(current.node)
		for edge, cost := range edges {
			if processed.Has(edge) {
				continue
			}

			pending = append(pending, pendingItem{node: edge, parent: current.node, cost: data[current.node].cost + int64(cost)})
		}

		processed.Add(current.node)
		pending = pending[1:]
	}

	return &DijkstraResult{data: data}
}
