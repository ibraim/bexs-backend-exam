package router

// Set represent a set of string values.
// A set has one or more unique string values, so adding the same
// value more than once to an existing set has no effect. A set
// also does not maintain the order in which items are added.
type Set struct {
	data map[string]struct{}
}

// NewSet returns an empty set.
func NewSet() *Set {
	return &Set{data: make(map[string]struct{})}
}

// Add insert the value on the set.
func (s *Set) Add(value string) {
	s.data[value] = struct{}{}
}

// Remove removs a value from the set.
func (s *Set) Remove(value string) {
	delete(s.data, value)
}

// Has returns whether the value is included in the set.
func (s *Set) Has(value string) bool {
	_, found := s.data[value]
	return found
}

// Size returns the size of the set.
func (s *Set) Size() int {
	return len(s.data)
}

// Slice returns a slice with the values of this set.
func (s *Set) Slice() []string {
	slice := make([]string, 0, len(s.data))

	for k := range s.data {
		slice = append(slice, k)
	}

	return slice
}
