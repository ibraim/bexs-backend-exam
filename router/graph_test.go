package router_test

import (
	"bexs-backend-exam/router"
	"sort"
	"testing"
)

func TestGraphNodes(t *testing.T) {
	graph := router.NewGraph()
	expected := []string{"A", "B", "C", "D"}

	graph.Add("A", "B", 1)
	graph.Add("B", "C", 1)
	graph.Add("A", "C", 1)
	graph.Add("A", "D", 1)
	graph.Add("C", "D", 1)

	nodes := graph.Nodes()
	if len(nodes) != len(expected) {
		t.Fatalf("Expected number of nodes to be %d, but is %d", len(expected), len(nodes))
	}

	sort.Strings(nodes)

	for i := 0; i < len(expected); i++ {
		if nodes[i] != expected[i] {
			t.Fatalf("Expected node to be '%s' but it was '%s'", expected[i], nodes[i])
		}
	}
}

func TestGraphEdges(t *testing.T) {
	graph := router.NewGraph()
	expected := []string{"B", "C", "D"}

	graph.Add("A", "B", 1)
	graph.Add("B", "C", 1)
	graph.Add("A", "C", 1)
	graph.Add("A", "D", 1)
	graph.Add("C", "D", 1)

	edgesOfA := graph.EdgesOf("A")
	if len(edgesOfA) != len(expected) {
		t.Fatalf("Expected number of edges to be %d but it was %d", len(expected), len(edgesOfA))
	}

	for _, node := range expected {
		if _, found := edgesOfA[node]; !found {
			t.Fatalf("Should have found and edge to '%s', but none found", node)
		}
	}

	edgesOfD := graph.EdgesOf("D")
	if len(edgesOfD) != 0 {
		t.Fatalf("Edges of node D should be zero, but it was %d", len(edgesOfD))
	}
}

func equal(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}

	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}

	return true
}

func TestDijkstra(t *testing.T) {
	expectedRoutes := []string{"GRU", "BRC", "SCL", "ORL", "CDG"}
	expectedCost := int64(40)

	graph := router.NewGraph()
	graph.Add("GRU", "BRC", 10)
	graph.Add("BRC", "SCL", 5)
	graph.Add("GRU", "CDG", 75)
	graph.Add("GRU", "SCL", 20)
	graph.Add("GRU", "ORL", 56)
	graph.Add("ORL", "CDG", 5)
	graph.Add("SCL", "ORL", 20)

	res := graph.Dijkstra("GRU")

	routes, cost, err := res.To("CDG")
	if err != nil {
		t.Fatal(err)
	}

	if !equal(routes, expectedRoutes) {
		t.Fatalf("Expected routes to be '%v' but received '%v'", expectedRoutes, routes)
	}

	if cost != expectedCost {
		t.Fatalf("Expected cost to be %d, but received %d", expectedCost, cost)
	}
}

func TestDijkstraErr(t *testing.T) {
	graph := router.NewGraph()
	graph.Add("GRU", "BRC", 10)
	graph.Add("BRC", "SCL", 5)
	graph.Add("GRU", "CDG", 75)
	graph.Add("GRU", "SCL", 20)
	graph.Add("GRU", "ORL", 56)
	graph.Add("ORL", "CDG", 5)
	graph.Add("SCL", "ORL", 20)

	res := graph.Dijkstra("SCL")
	_, _, err := res.To("GRU")

	if err != router.ErrUnreachable {
		t.Fatalf("Expected error '%v', but received '%v'", router.ErrUnreachable, err)
	}
}
