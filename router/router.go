package router

import (
	"bufio"
	"fmt"
	"io"
	"strconv"
	"strings"
	"sync"
)

// Router manages the route information of the app.
// It is possible to add routes and to compute the shortest
// path between nodes.
type Router struct {
	graph         *Graph
	dijkstraCache map[string]*DijkstraResult
	lock          sync.RWMutex
}

// New returns a new Router.
func New() *Router {
	return &Router{graph: NewGraph(), dijkstraCache: make(map[string]*DijkstraResult)}
}

// Add adds and edge between two nodes, with a given cost.
// If this edge already exists, it is replaced.
func (r *Router) Add(fromNode, toNode string, cost int) {
	r.lock.Lock()
	r.dijkstraCache = make(map[string]*DijkstraResult)
	r.graph.Add(fromNode, toNode, cost)
	r.lock.Unlock()
}

// GetRoute returns the route and the associated cost between two nodes
// in the graph.
func (r *Router) GetRoute(fromNode, toNode string) ([]string, int64, error) {
	r.lock.Lock()
	result, found := r.dijkstraCache[fromNode]
	if !found {
		result = r.graph.Dijkstra(fromNode)
		r.dijkstraCache[fromNode] = result
	}
	r.lock.Unlock()

	return result.To(toNode)
}

// LoadFrom loads route information from a CSV file.
func (r *Router) LoadFrom(reader io.Reader) error {
	r.lock.Lock()
	defer r.lock.Unlock()

	r.graph = NewGraph()
	r.dijkstraCache = make(map[string]*DijkstraResult)
	scanner := bufio.NewScanner(reader)

	for scanner.Scan() {
		line := scanner.Text()
		if line == "" {
			continue
		}

		fields := strings.Split(line, ",")

		if len(fields) != 3 {
			return fmt.Errorf("incorrect field numbers on line '%s'", line)
		}

		val, err := strconv.ParseInt(fields[2], 10, 0)
		if err != nil {
			return fmt.Errorf("error parsing line '%s': %w", line, err)
		}

		r.graph.Add(fields[0], fields[1], int(val))
	}

	if err := scanner.Err(); err != nil {
		return err
	}

	return nil
}

// SaveTo saves route information to a CSV file.
func (r *Router) SaveTo(writer io.Writer) error {
	r.lock.RLock()
	defer r.lock.RUnlock()

	nodes := r.graph.Nodes()

	for _, nodeA := range nodes {
		edges := r.graph.EdgesOf(nodeA)
		for nodeB, cost := range edges {
			if _, err := fmt.Fprintf(writer, "%s,%s,%d\n", nodeA, nodeB, cost); err != nil {
				return err
			}
		}
	}

	return nil
}
