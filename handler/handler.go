package handler

import (
	"bexs-backend-exam/router"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)

type routeInput struct {
	From string `json:"from"`
	To   string `json:"to"`
	Cost int    `json:"cost"`
}

type routeResult struct {
	Path []string `json:"path"`
	Cost int64    `json:"cost"`
}

type messageResult struct {
	Message string `json:"message"`
	IsError bool   `json:"isError"`
}

// Handler holds the needed state to run the various HTTP handlers of the API.
type Handler struct {
	filename string
	router   *router.Router
}

// New returns a new Handler, using the specified router and saving the changes to
// filename.
func New(filename string, router *router.Router) *Handler {
	return &Handler{filename, router}
}

func (h *Handler) writeJSONOrFail(w http.ResponseWriter, response interface{}) {
	w.Header().Add("content-type", "application/json")

	jsonBytes, err := json.Marshal(response)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		if _, err := w.Write([]byte(err.Error())); err != nil {
			fmt.Fprintln(os.Stderr, err)
		}

		return
	}

	w.WriteHeader(http.StatusOK)
	if _, err := w.Write(jsonBytes); err != nil {
		fmt.Fprintln(os.Stderr, err)
	}
}

func (h *Handler) methodNotAllowed(w http.ResponseWriter) {
	w.WriteHeader(http.StatusMethodNotAllowed)
	if _, err := w.Write([]byte("method not allowed")); err != nil {
		fmt.Fprintln(os.Stderr, err)
	}
}

// AddRoute handles the HTTP request to add a new route on the router.
func (h *Handler) AddRoute(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		h.methodNotAllowed(w)
		return
	}

	bodyBytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		h.writeJSONOrFail(w, messageResult{Message: err.Error(), IsError: true})
		return
	}

	var input routeInput
	if err := json.Unmarshal(bodyBytes, &input); err != nil {
		h.writeJSONOrFail(w, messageResult{Message: err.Error(), IsError: true})
		return
	}

	h.router.Add(input.From, input.To, input.Cost)

	file, err := os.Create(h.filename)
	if err != nil {
		h.writeJSONOrFail(w, messageResult{Message: err.Error(), IsError: true})
		return
	}
	defer func() {
		if err := file.Close(); err != nil {
			fmt.Fprintf(os.Stderr, "error closing file: %v\n", err)
		}
	}()

	if err := h.router.SaveTo(file); err != nil {
		h.writeJSONOrFail(w, messageResult{Message: err.Error(), IsError: true})
		return
	}

	h.writeJSONOrFail(w, messageResult{Message: "OK"})
}

// FindRoute handles the find route HTTP request.
func (h *Handler) FindRoute(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		h.methodNotAllowed(w)
		return
	}

	splitted := strings.Split(r.URL.String(), "/")
	if len(splitted) != 4 {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	nodes := strings.Split(splitted[3], "-")
	if len(nodes) != 2 {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	route, cost, err := h.router.GetRoute(nodes[0], nodes[1])
	if err != nil {
		h.writeJSONOrFail(w, messageResult{Message: err.Error(), IsError: true})
		return
	}

	h.writeJSONOrFail(w, routeResult{Path: route, Cost: cost})
}
