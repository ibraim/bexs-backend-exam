FROM golang:1.14 as stage

WORKDIR /app
COPY . .
RUN go build -ldflags '-w -s' -o bexs-backend-exam

FROM golang:1.14
WORKDIR /app
COPY --from=stage /app/bexs-backend-exam .
COPY --from=stage /app/input-file.txt .
CMD ["./bexs-backend-exam", "input-file.txt"]
